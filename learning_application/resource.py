#
# Copyright (C) 2019-2021 Guillaume Bernard <guillaume.bernard@koala-lms.org>
#
# Copyright (C) 2021 Samuel Vannier <samuelvannier@hotmail.fr>
#
# This file is part of Koala LMS (Learning Management system)

# Koala LMS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from learning_application.enums import OrderedEnum
from learning_application import gettext as _


class ResourceAccess(OrderedEnum):
    """
    Defines the different access rights for resources.
    """

    PUBLIC = (_("Public"), 0)
    EXISTING_ACTIVITIES = (_("Only in activities"), 1)
    COLLABORATORS_ONLY = (_("Collaborators only"), 2)
    PRIVATE = (_("Private"), 3)


class ResourceReuse(OrderedEnum):
    """
    Defines the different reuse rights for resources.
    """

    NO_RESTRICTION = (_("Reusable"), 0)
    ONLY_AUTHOR = (_("Author only"), 1)
    NON_REUSABLE = (_("Non reusable"), 2)
