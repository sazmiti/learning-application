#!/usr/bin/env python3

import os

from setuptools import find_packages, setup

with open(
    os.path.join(os.path.dirname(__file__), "README.md"), encoding="utf-8"
) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


def get_requirements():
    with open("requirements.txt") as requirements:
        req = requirements.read().splitlines()
    return req


exec(open("learning_application/__init__.py").read())

# noinspection PyUnresolvedReferences
setup(
    name="koalalms-learning-application",
    version="0.1",
    packages=find_packages(),
    include_package_data=True,
    license="GPLv3 License",
    description="The Koala-LMS application to manage courses, activities and educational resources.",
    long_description=README,
    long_description_content_type="text/markdown",
    author="Guillaume Bernard",
    author_email="guillaume.bernard@koala-lms.org",
    install_requires=get_requirements(),
)
