#
# Copyright (C) 2019-2021 Guillaume Bernard <guillaume.bernard@koala-lms.org>
#
# Copyright (C) 2021 Samuel Vannier <samuelvannier@hotmail.fr>
#
# This file is part of Koala LMS (Learning Management system)

# Koala LMS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from enum import Enum
from learning_application import gettext as _


class OrderedEnum(Enum):
    """
    Special enumeration which can has ordered elements. Each literal has a weight which is used to compare with others.

    .. warning:: Literals are tuples with a value and a weight, like:
        A = ("A", 0)
        B = ("B", 1
    """

    # pylint: disable=unused-argument
    # noinspection PyUnusedLocal
    def __init__(self, token: str, weight: int):
        self.__weight = weight

    @property
    def weight(self) -> int:
        """
        Get the enumeration literal weight.

        :return: the enumeration literal weight
        :rtype: int
        """
        return self.__weight

    @property
    def value(self) -> str:
        """
        Get the enumeration literal value (the string part of the tuple)

        :return: the enumeration literal value
        :rtype: str
        """
        return super().value[0]

    def __eq__(self, other):
        return self.weight == other.weight

    def __lt__(self, other):
        return self.weight < other.weight

    def __gt__(self, other):
        return self.weight > other.weight

    def __le__(self, other):
        return self.weight <= other.weight

    def __ge__(self, other):
        return self.weight >= other.weight


class Licences(OrderedEnum):
    """
    Authorised licences that can be used in resources. The elements are ordered according to the degree of freedom to
    use content.
    """

    CC_0 = (_("Creative Commons 0 (Public domain)"), 0)
    CC_BY = (_("Creative Commons Attribution"), 1)
    CC_BY_SA = (_("Creative Commons Attribution, Share Alike"), 2)
    CC_BY_NC = (_("Creative Commons Attribution, Non Commercial"), 3)
    CC_BY_NC_SA = (_("Creative Commons Attribution, Non Commercial, Share Alike"), 4)
    CC_BY_ND = (_("Creative Commons Attribution, No Derivatives"), 5)
    CC_BY_NC_ND = (_("Creative Commons Attribution, No Commercial, No Derivatives"), 6)
    NA = (_("Not appropriate"), 7)
    ALL_RIGHTS_RESERVED = (_("All rights reserved"), 8)


class Duration(OrderedEnum):
    """
    The different allowed duration that are attached to elements to indicate how long it takes to view them.
    """

    FIVE_OR_LESS = (_("Less than 5 minutes"), 0)
    TEN_OR_LESS = (_("Less than 10 minutes"), 0)
    FIFTEEN_OR_LESS = (_("Less than 15 minutes"), 1)
    TWENTY_OR_LESS = (_("Less than 20 minutes"), 1)
    TWENTY_FIVE_OR_LESS = (_("Less than 25 minutes"), 2)
    THIRTY_OR_MORE = (_("30 minutes or more"), 2)
    NOT_SPECIFIED = (_("Not specified"), 3)
